package com.example.gameguild.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Base64
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator

import com.example.gameguild.R
import com.example.gameguild.activities.FullNewsActivity
import com.example.gameguild.adapters.NewsListAdapter
import com.example.gameguild.data.datamodel.News
import com.example.gameguild.interfaces.ItemClickListener
import com.example.gameguild.interfaces.PaginationListener
import com.example.gameguild.network.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [NewsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NewsFragment : Fragment(), PaginationListener {

    var apiManager : ApiManger? = null
    val handler : Handler = Handler(Looper.getMainLooper())
    val pageSize : Int = 5

    lateinit var newsAdapter : NewsListAdapter
    lateinit var newsRecyclerView : RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private fun initView(view : View) {
        newsRecyclerView = view.findViewById(R.id.fragment_news_list)
        newsAdapter = context?.let { NewsListAdapter(it, this, {item ->  onItemClicked(item as String)}) }!!

        val animator: RecyclerView.ItemAnimator? = newsRecyclerView.itemAnimator
        if (animator is SimpleItemAnimator) {
            animator.supportsChangeAnimations = false
        }
        newsRecyclerView.adapter = newsAdapter
        newsRecyclerView.layoutManager = LinearLayoutManager(context)

        apiManager = ApiManger().getInstance()
    }

    private fun getFirstNewsDataPage() {
        apiManager?.getMainNews(object : ApiResponseReceived{
            override fun onResponseReceived(what: Int, response: Any) {
//                TODO("Not yet implemented")
                when(what){
                    GET_NEWS_LIST_SUCCESSFULL -> {
                        var listNews = response as List<News>
                        newsAdapter.setData(listNews, decodeImage(listNews))
                        Log.d("Success", " listSize " + listNews.size)
                    }

                    GET_NEWS_LIST_UNSUCCESSFULL -> {
                        var error = response as String
                        Log.d("Success", " listSize " + error)
                    }
                }
            }
        }, 0,pageSize)
    }

    override fun onLoadNext(nextPageNumber: Int) {
        apiManager?.getMainNews(object : ApiResponseReceived{
            override fun onResponseReceived(what: Int, response: Any) {
//                TODO("Not yet implemented")
                when(what){
                    GET_NEWS_LIST_SUCCESSFULL -> {
                        var listNews = response as List<News>
                        newsAdapter.addData(listNews, decodeImage(listNews))
                        Log.d("Success", " listSize " + listNews.size)
                    }

                    GET_NEWS_LIST_UNSUCCESSFULL -> {
                        var error = response as String
                        Log.d("Success", " listSize " + error)
                    }
                }
            }
        }, nextPageNumber,pageSize)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_news, container, false)
        initView(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getFirstNewsDataPage()
    }

    private fun decodeImage(newsList: List<News>): MutableMap<String, ByteArray> {
        var newsHashMap: MutableMap<String, ByteArray> = mutableMapOf()
        for (item in newsList) {
            val imageStr = item.mainImage
            var decodedString: ByteArray = byteArrayOf()
            if (imageStr != null){
                decodedString = Base64.decode(imageStr, Base64.DEFAULT)
            }
            newsHashMap[item._id] = decodedString
        }
        return newsHashMap
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NewsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NewsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun onItemClicked(item: String) {
        val i = item
        startActivity(context?.let { FullNewsActivity.newIntent(it, item) })
    }
}
