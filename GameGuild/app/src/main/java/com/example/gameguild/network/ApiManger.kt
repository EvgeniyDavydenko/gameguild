package com.example.gameguild.network

import android.util.Log
import com.example.gameguild.data.datamodel.FullNews
import com.example.gameguild.data.datamodel.News
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiManger {
    private var apiManagerInstance : ApiManger? = null

    fun getInstance(): ApiManger {
        if (apiManagerInstance == null) {
            apiManagerInstance = ApiManger()
        }
        return apiManagerInstance as ApiManger
    }

    fun getMainNews(listener: ApiResponseReceived, pageNumber: Int, pageSize: Int) {
        val apiInterface = getClient()
        val call: Call<List<News>> = apiInterface.getAllNews(pageNumber, pageSize)
        Log.d("ApiManger", "CurrentThread " + Thread.currentThread())
        call.enqueue(object : Callback<List<News>> {
            override fun onResponse(call: Call<List<News>>, response: Response<List<News>>) {
//                TODO("Not yet implemented")
                if (response.isSuccessful) {
                    response.body()?.let {
                        listener.onResponseReceived(
                            GET_NEWS_LIST_SUCCESSFULL,
                            it
                        )
                    }
                } else {
                    response.errorBody()?.let {
                        listener.onResponseReceived(
                            GET_NEWS_LIST_UNSUCCESSFULL,
                            it
                        )
                    }
                }
            }

            override fun onFailure(call: Call<List<News>>, t: Throwable) {
//                TODO("Not yet implemented")
                t.message?.let { listener.onResponseReceived(GET_NEWS_LIST_UNSUCCESSFULL, it) }
            }
        })
    }

    fun getFullNews(listener: ApiResponseReceived, newsID : String){
        val apiInterface = getClient()
        val call : Call<FullNews> = apiInterface.getFullNews(newsID)
        call.enqueue(object: Callback<FullNews>{
            override fun onResponse(call: Call<FullNews>, response: Response<FullNews>) {
//                TODO("Not yet implemented")
                if (response.isSuccessful){
                    response.body()?.let {
                        listener.onResponseReceived(GET_FULL_NEWS_SUCCESSFULL, it)
                    }
                } else {
                    response.errorBody()?.let{
                        listener.onResponseReceived(GET_FULL_NEWS_UNSUCCESSFULL, it)
                    }
                }
            }

            override fun onFailure(call: Call<FullNews>, t: Throwable) {
//                TODO("Not yet implemented")
                t.message?.let{listener.onResponseReceived(GET_FULL_NEWS_UNSUCCESSFULL, it)}
            }
        })
    }

    suspend fun getFullNewsCoroutines(newsId: String): Response<FullNews> {
        val apiInterface = getClient()
        return apiInterface.getFullNewsCoroutines(newsId)
    }
}