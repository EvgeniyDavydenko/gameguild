package com.example.gameguild.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.gameguild.R
import com.example.gameguild.fragments.NewsFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
    }

    private fun initView() {
        val fragmentManager = supportFragmentManager
        var fragment = fragmentManager.findFragmentById(R.id.newsListFrame)
        if (fragment == null) {
            fragment = NewsFragment()
            fragmentManager.beginTransaction()
                .add(R.id.newsListFrame, fragment)
                .commit()
        }
    }
}
