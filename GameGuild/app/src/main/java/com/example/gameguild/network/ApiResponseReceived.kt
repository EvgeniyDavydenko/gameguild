package com.example.gameguild.network

interface ApiResponseReceived {
    fun onResponseReceived(what : Int, response : Any)
}