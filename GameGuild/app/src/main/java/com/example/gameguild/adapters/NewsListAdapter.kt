package com.example.gameguild.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.gameguild.R
import com.example.gameguild.data.datamodel.News
import com.example.gameguild.interfaces.PaginationListener

class NewsListAdapter(context: Context, _paginationlistener: PaginationListener,
                     val itemClickListener: (String) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var newsList: MutableList<News> = ArrayList()
    private var imagesHashMap: MutableMap<String, ByteArray> = mutableMapOf()
    private lateinit var newsitem: News
//    private lateinit var itemClickListener: ItemClickListener<String>



    lateinit var newsRecyclerView: RecyclerView
    lateinit var paginationListener : PaginationListener
    private var isLastDataPage : Boolean = false
    private var nextPageNumber : Int = 1;

    init {
        paginationListener = _paginationlistener
//        itemClickListener = _itemClickListener
    }

    override fun getItemCount(): Int {
        return newsList.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = layoutInflater.inflate(R.layout.news_item, parent, false)
        return NewsViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is NewsViewHolder) {
            newsitem = newsList[position]
            val newsViewHolder: NewsViewHolder = holder
            newsViewHolder.tvNewsTitle?.text = newsitem.headOfNews

            if (imagesHashMap.containsKey(newsitem._id)) {
                Glide.with(layoutInflater.context)
                    .load(imagesHashMap[newsitem._id])
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(false)
                    .dontAnimate()
                    .into(newsViewHolder.ivImageView)
            }
        }

        if (position == newsList.size - 1 && !isLastDataPage) {
            paginationListener.onLoadNext(nextPageNumber)
        }
    }

    fun setData(_newsList: List<News>, _imagesHashMap: MutableMap<String, ByteArray>) {
        newsList.clear()
        imagesHashMap.clear()
        newsList.addAll(_newsList)
        imagesHashMap = _imagesHashMap
        notifyDataSetChanged()
    }

    fun addData(nextNewsList: List<News>, nextImagesHashMap: MutableMap<String, ByteArray>) {
        if (nextNewsList.isEmpty()) {
            isLastDataPage = true
            notifyDataSetChanged()
            return
        }
        imagesHashMap.putAll(nextImagesHashMap)
        nextPageNumber++
        newsList.addAll(nextNewsList)
        notifyDataSetChanged()
    }


    private inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivImageView: ImageView
        var tvNewsTitle: TextView? = null

        init {
            ivImageView = itemView.findViewById(R.id.news_picture)
            tvNewsTitle = itemView.findViewById(R.id.news_title)

            itemView.setOnClickListener {
                itemClickListener(newsList[this.adapterPosition]._id)
            }
        }
    }
}