package com.example.gameguild.data.datamodel

data class News(
    val UUID: Long,
    val _id: String,
    val headOfNews: String,
    val isHot: Boolean,
    val mainImage: String,
    val mark: Mark
) {
    data class Mark(val like: Int, val dislike: Int)
}