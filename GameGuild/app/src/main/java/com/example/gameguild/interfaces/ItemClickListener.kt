package com.example.gameguild.interfaces

interface ItemClickListener<T> {
    fun OnItemClicked(item: T)
}