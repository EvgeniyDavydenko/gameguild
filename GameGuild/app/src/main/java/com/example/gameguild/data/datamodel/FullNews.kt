package com.example.gameguild.data.datamodel

data class FullNews(
    val _id: String,
    val headOfNews: String,
    val text: String,
    val genre: List<String>,
    val tags: List<String>,
    val category: List<String>,
    val mainImage: String
) {
}