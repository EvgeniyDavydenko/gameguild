package com.example.gameguild.interfaces

interface PaginationListener {
    fun onLoadNext(nextPageNumber: Int)
}