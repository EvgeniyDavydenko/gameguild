package com.example.gameguild.activities

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.gameguild.R
import com.example.gameguild.data.datamodel.FullNews
import com.example.gameguild.network.ApiManger
import com.example.gameguild.network.ApiResponseReceived
import com.example.gameguild.network.GET_FULL_NEWS_SUCCESSFULL
import com.example.gameguild.network.GET_FULL_NEWS_UNSUCCESSFULL
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class FullNewsActivity : AppCompatActivity() {

    lateinit var newsImage: ImageView
    lateinit var newsTitle: TextView
    lateinit var newsBody: TextView

    companion object {
        fun newIntent(context: Context, newsID: String): Intent {
            val intent = Intent(context, FullNewsActivity::class.java)
            intent.putExtra("newsID", newsID)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_news)

        initView()
        Log.d("Test coroutines", "before start")
        intent?.getStringExtra("newsID")?.let { getFullNewsCoroutines(it) }
        Log.d("Test coroutines", "after execute")
    }

    private fun initView() {
        newsImage = findViewById(R.id.news_picture)
        newsTitle = findViewById(R.id.news_title)
        newsBody = findViewById(R.id.news_body)
    }

    private fun getFullNews(newsId : String) {
        val apiManager = ApiManger().getInstance()
        apiManager.getFullNews(object : ApiResponseReceived {
            override fun onResponseReceived(what: Int, response: Any) {
    //                TODO("Not yet implemented")
                when(what){
                    GET_FULL_NEWS_SUCCESSFULL -> {
                        var fullNewsData = response as FullNews
                        showNewsData(fullNewsData)
                        Log.d("Success", " fullNewsData loaded ")
                    }

                    GET_FULL_NEWS_UNSUCCESSFULL -> {
                        var error = response as String
                        Log.d("Success", " fullNewsData loading error " + error)
                    }
                }
            }
        }, newsId)
    }

    private fun getFullNewsCoroutines(newsId: String) {
        val apiManager = ApiManger().getInstance()
        GlobalScope.launch(Dispatchers.Main) {
            val response = apiManager.getFullNewsCoroutines(newsId)
            Log.d("Test coroutines", "inside")
            if (response.isSuccessful) {
                val fullNewsData = response.body() as FullNews
                showNewsData(fullNewsData)
                Log.d("Success", " fullNewsData loaded ")
            } else {
                Log.d("Success", " fullNewsData loading error " + response.errorBody())
            }
        }
    }

    private fun showNewsData(fullNews: FullNews) {
        newsTitle.text = fullNews.headOfNews
        newsBody.text = fullNews.text

        val imageStr = fullNews.mainImage
        var decodedString: ByteArray = byteArrayOf()
        imageStr.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                decodedString = Base64.getDecoder().decode(imageStr)
            }
        }

        Glide.with(newsImage.context)
            .load(decodedString)
            .into(newsImage)
    }
}
