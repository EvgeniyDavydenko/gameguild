package com.example.gameguild.network

import com.example.gameguild.data.datamodel.FullNews
import com.example.gameguild.data.datamodel.News
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("gg/api/news/getmainnews/{pageNumber}/{pageSize}")
    fun getAllNews(@Path ("pageNumber") pageNumber : Int, @Path("pageSize") pageSize : Int) : Call<List<News>>

    @GET("gg/api/news/getFullNews/{id}")
    fun getFullNews(@Path("id") newsID: String): Call<FullNews>

    @GET("gg/api/news/getFullNews/{id}")
    suspend fun getFullNewsCoroutines(@Path("id") newsID: String): Response<FullNews>
}